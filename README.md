# Postmark Test Settings
**WARNING: This plugin should not be used unless the official Postmark plugin is active!**

## Description
This is a WordPress plugin that is meant to add test functionality to the [official Postmark WordPress plugin](https://wordpress.org/plugins/postmark-approved-wordpress-plugin/).  Currently, the plugin allows a site owner to override the default recipient of any email sent through wp-mail (and therefore Postmark). This allows safe testing of staging and development sites where email functionality may be expected to work, but not send emails to any real users.

### Why not just add this functionality to a site's functions.php file?
Because most WordPress sites are hacky enough as it is! At my job I manage a lot of WordPress sites and the last thing I want to do is add a hard-coded hack to each one, this plugin allows me to safely test each site in an automated fashion.

## Installation
This plugin is meant to be installed via [Composer](https://getcomposer.org/). For details on how Composer can be used with Wordpress see this [documentation](https://wpackagist.org/).

## Special Thanks
A very big thank you to [David Hayes at WPShout!](https://wpshout.com/making-an-admin-options-page-with-the-wordpress-settings-api/) for explaining WordPress's nightmarish Settings API.
