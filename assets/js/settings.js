(function() {
	var enable_selector
		= document.querySelector('input[name="pmts_enabled"]');
	var default_recipient_selector
    = document.querySelector('input[name="pmts_default_recipient"]');
  var enable_test_header
    = document.querySelector('input[name="pmts_enable_test_api_key"]');

	enable_selector.addEventListener('click', function() {
    default_recipient_selector.disabled = !enable_selector.checked;
    enable_test_header.disabled = !enable_selector.checked;
  }, false);

  if (!enable_selector.checked) {
    default_recipient_selector.disabled = true;
    enable_test_header.disabled = true;
  }
})();
